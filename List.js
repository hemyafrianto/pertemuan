/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan.store.Personnel'
    ],

    title: 'Data Anggota Sencha Study Club',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'NPM',  dataIndex: 'npm', width: 100 },
        { text: 'Nama',  dataIndex: 'name', width: 100 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Telepon', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onDataDipilih'
    }
});
